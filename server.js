let express = require('express');
let bodyParser = require('body-parser');
let app = express();
let port = process.env.PORT || 3005;
let cors = require('cors');
let routes = require('./routes');
app.use(bodyParser.json());
routes(app);
app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
