const cors = require('cors');

const mysql = require('mysql');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'chat',
    table: 'messages'
});

module.exports = function (app) {
    app.use(cors());
    
    app.get('/messages', function (req, res) {
        connection.query('SELECT * FROM messages', function (error, results, fields) {
            if (error) throw error;
            res.send(results);
        }
        );
    });

    app.post('/newmessage', function (req, res) {
        connection.query('INSERT INTO messages SET ?', req.body, function (error, results, fields) {
            if (error) throw error;
            res.send(results);
         }
        );
    });
}